var Twit = require('twit');
var config = require('./config');
var http = require('http');
var url = require('url');
var fs = require('fs');
var express = require('express');
var mongoose= require('mongoose');
var Twits = require('./schema/twits');
var app= express();

var router= express.Router();

router.get('/',function(req,res){
	fs.readFile("./instructions.html",null, function (error, pgResp) {
            if (error) {
                res.writeHead(404);
                res.write('Contents you are looking are Not Found');
            } 
			else {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(pgResp);
            }
             
            res.end();
        });
})

router.get('/bydate',function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	//console.log(que);
	Twits.find({})
	.where('created_at_date').gt(que.Fromdate).lt(que.Todate)
	.sort({'created_at_date': que.Order})
	.exec(function(err,docs){
		if(err)
		{
			throw(err);
		}
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text', 'user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
	});
	
});

router.get('/byname/contains', function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	//console.log(que);
	var str= "/"+que.Name +"/i";
	console.log(str);
	Twits.find({'user.name': {"$regex":que.Name, "$options": "i"}})
	.exec(function(err,docs){
		if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
	});
	
});
router.get('/byname/starts',function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	//console.log(que);
	var str= '^'+ que.Name ;
	//console.log(str);
	Twits.find({'user.name':{ "$regex":str, "$options": "i" }})
	.exec(function(err,docs){
		if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
	});
	
});
router.get('/byname/ends',function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	//console.log(que);
	var str= que.Name +'$';
	//console.log(str);
	Twits.find({'user.name':{ "$regex":str, "$options": "i" }})
	.exec(function(err,docs){
		if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
	});
	
});
router.get('/byretweet',function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	//console.log(que);
	if(que.LGE == 'L')
	{
		Twits.find({'retweet_count': {"$lt": que.Count}})
		.sort({'retweet_count' : -1})
		.exec(function(err,docs){
			if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
		});
	}
	else if(que.LGE == 'G')
	{
		Twits.find({'retweet_count': {"$gt": que.Count}})
		.sort({'retweet_count' : 1})
		.exec(function(err,docs){
			if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
		});
	}
	else if(que.LGE == 'E')
	{
		Twits.find({'retweet_count': que.Count})
		.exec(function(err,docs){
				if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
		})
	}
	
});

router.get('/byfav',function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	//console.log(que);
	if(que.LGE == 'L')
	{
		Twits.find({'favorite_count': {"$lt": que.Count}})
		.sort({'favorite_count' : -1})
		.exec(function(err,docs){
			if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
		});
	}
	else if(que.LGE == 'G')
	{
		Twits.find({'favorite_count': {"$gt": que.Count}})
		.sort({'favorite_count' : 1})
		.exec(function(err,docs){
			if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
		});
	}
	else if(que.LGE == 'E')
	{
		Twits.find({'favorite_count': que.Count})
		.exec(function(err,docs){
				if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
		});
	}
	
});

router.get('/bylang',function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	console.log(que);
	Twits.find({'lang': que.Lang})
	.sort({'created_at_date': -1})
	.exec(function(err,docs){
		if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.name', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
	});
});

router.get('/byurl/contains', function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	//console.log(que);
	//var str= "/"+que.Url +"/i";
	//console.log(str);
	Twits.find({'user.url': {"$regex":que.Url, "$options": "i"}})
	.exec(function(err,docs){
		if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.url', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
	});
	
});

router.get('/byurl', function(req,res){
	res.writeHead(200, {'Content-Type': 'text/csv'});
	var que = url.parse(req.url, true).query; 
	//console.log(que);
	//var str= "/"+que.Url +"/i";
	//console.log(str);
	Twits.find({'user.url': que.Url})
	.exec(function(err,docs){
		if(err)
		{
			throw(err);
		}
		//console.log(docs);
		var json2csv = require('json2csv');
		var fields = ['created_at', 'id', 'text','user.url', 'truncated', 'source', 'in_reply_to_status_id', 'in_reply_to_user_id', 'in_reply_to_screen_name', 'is_quote_status', 'retweet_count', 'favorite_count', 'favorited', 'retweeted', 'possibly_sensitive', 'lang'];
 
		try {
			var result = json2csv({ data:docs, fields: fields });
			//console.log(result);
		} catch (err) {
			// Errors are thrown for bad options, or if the data is empty and no fields are provided.
			// Be sure to provide fields if it is possible that your data array will be empty.
			console.error(err);
		}
		
		res.write(result);
		res.end();
	});
	
});

module.exports= router;