var mongoose = require('mongoose');

var Schema = mongoose.Schema;
// 
var TwitsSchema = new Schema(
	{
		created_at: {type: String, required: true},
		created_at_date: {type: Date, required: true, default: '1997-07-30 23:59:59.000'},
       id: {type: Number, required: true},
       id_str: {type: String, required: true},
       text: {type: String, required: true},
       truncated: {type: Boolean, required: true},
       source: {type: String, required: true},
       in_reply_to_status_id: {type: Number, default: null},
       in_reply_to_status_id_str: {type: String, default: null},
       in_reply_to_user_id: {type: Number, default: null},
       in_reply_to_user_id_str: {type: String, default: null},
       in_reply_to_screen_name: {type: String, default: null},
       user: {type: {
		   id: Number,
		   id_str: String,
		   name: String,
		   screen_name: String,
		   created_at: String,
		   url: {type: String, default: null}
	   }
	   , default: null},
       geo: {type:{
		   id: String,
		   url: String,
		   place_type: String,
		   name: String,
		   full_name: String
	   }
	   , default: null},
       coordinates: {type: [Number],default: null},
       place: {type: {
		   id: String,
		   name: String,
		   full_name: String
	   }, default: null},
       is_quote_status: {type: Boolean},
       retweet_count: {type: Number},
       favorite_count: {type: Number},
       favorited: {type: Boolean, default: false},
       retweeted: {type: Boolean, default: false},
       possibly_sensitive: {type: Boolean, default: false},
       lang: {type: String, required: true} 
	   
	}
	
);
TwitsSchema.index({id: 1, text: 1}, {unique: true});

TwitsSchema
.virtual('url')
.get(function () {
	return '/API1/twits/' + this._id;
});

module.exports = mongoose.model('Twits', TwitsSchema);