# HackerCamp_Backend

Hackercamp backend assignment.
Run API1 with example url "localhost:8080/search?Query=<query>"
to search for 100 tweets containing given query and store them in database.
Run API2 with example url "localhost:8080/filter"
and follow instruction displayed to download CSV filtered data from DB.