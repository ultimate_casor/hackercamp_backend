var Twit = require('twit');
var config = require('./config');
var http = require('http');
var url = require('url');
var fs = require('fs');
var express = require('express');
var app= express();
//Import the mongoose module
var mongoose = require('mongoose');
var cnt=100;


//Set up default mongoose connection
mongoose.connect('mongodb://localhost:27017/twiterdb');
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

db.once('open', function() {
  // we're connected!
	

var Twits = require('./schema/twits');


// set up twitter connection.

var T = Twit(config);

app.get('/search',function(req,res){
	//res.writeHead(200, {'Content-Type': 'text/html'});				//specifying write head;
	var que = url.parse(req.url, true).query;
	//console.log(que);
	

	var params = {
	q: que.Query,
	count: cnt	
	};

	T.get('search/tweets', params, gotData);
	/*res.write("Tweets with query:"+que.Query+"are stored");
	res.write("use \"localhost:8080/filter/bydate?Order=1\\0&Fromdate=\"2018-02-12T18:33:00.000\"&Todate=\"2018-02-12T18:33:00.000\" to search by date applyig range as well");
	res.write("use \"localhost:8080/filter/byname/contains?Name=Neeraj\" to search for tweets  containing Name");
	res.write("use \"localhost:8080/filter/byname/starts?Name=Neeraj\" to search for tweets starts with Name");
	res.write("use \"localhost:8080/filter/byretweet?LGE=L&Count=10\" to serach for tweets with retweets 'L' for less than 'G' for greater than and E for equals to Count");
	res.write("use \"localhost:8080/filter/byfav?LGE=L&Count=10\" to serach for tweets with favorite_count 'L' for less than, 'G' for greater, than and E for equals to Count");
	res.write("use \"localhost:8080/filter/bylang?Lang=hi\" to search for tweets in langague with code Lang");
	*/
	res.send('OK');
	
	
	
});

function gotData(err,data,res){
	//console.log(data);
	
	for( var i=0;i<data.statuses.length;i++ )
	{
		var msg= 'saving tweet no. '+ (i+1);
		console.log(msg);
		
		var newTweet = new Twits(
		{
			created_at: data.statuses[i].created_at,
			id: data.statuses[i].id,
			id_str: data.statuses[i].id_str,
			text: data.statuses[i].text,
			truncated: data.statuses[i].truncated,
			source: data.statuses[i].source,
			in_reply_to_status_id: data.statuses[i].in_reply_to_status_id,
			in_reply_to_status_id_str: data.statuses[i].in_reply_to_status_id_str,
			in_reply_to_user_id: data.statuses[i].in_reply_to_user_id,
			in_reply_to_user_id_str: data.statuses[i].in_reply_to_user_id_str,
			in_reply_to_screen_name: data.statuses[i].in_reply_to_screen_name,
			user: {
		   id: data.statuses[i].user.id,
		   id_str: data.statuses[i].user.id_str,
		   name: data.statuses[i].user.name,
		   screen_name: data.statuses[i].user.screen_name,
		   created_at: data.statuses[i].user.created_at,
		   url: data.statuses[i].user.url
	   },
	   is_quote_status: data.statuses[i].is_quote_status,
       retweet_count: data.statuses[i].retweet_count,
       favorite_count: data.statuses[i].favorite_count,
       favorited: data.statuses[i].favorited,
       retweeted: data.statuses[i].retweeted,
       possibly_sensitive: data.statuses[i].possibly_sensitive,
       lang: data.statuses[i].lang 
		});
	  if( data.statuses[i].geo != null )
	  { 
       newTweet.geo= {
		   id: data.statuses[i].geo.id,
		   url: data.statuses[i].geo.url,
		   place_type: data.statuses[i].geo.place_type,
		   name: data.statuses[i].geo.name,
		   full_name: data.statuses[i].geo.full_name
	   }
	  }
	    if( data.statuses[i].coordinates != null )
		{
			newTweet.coordinates= data.statuses[i].coordinates.coordinates
		}
        if( data.statuses[i].place != null )
		{
	   newTweet.place= {
		   id: data.statuses[i].place.id,
		   name: data.statuses[i].place.name,
		   full_name: data.statuses[i].place.full_name
	   }
		}
		var str=data.statuses[i].created_at;
		var l=str.length;
		var arr= str.split(" ");
		var month='0';
		switch(arr[1]){
			case 'Jan':
				month='1';
				break;
			case 'Feb':
				month='2';
				break;
			case 'Mar':
				month='3';
				break;
			case 'Apr':
				month='4';
				break;
			case 'May':
				month='5';
				break;
			case 'Jun':
				month='6';
				break;
			case 'Jul':
				month='7'
				break;
			case 'Aug':
				month='8';
				break;
			case 'Sep':
				month='9';
				break;
			case 'Oct':
				month='10';
				break;
			case 'Nov':
				month='11';
				break;
			case 'Dec':
				month='12';
				break;
		} 
		var pushdate= arr[5]+'-'+month+'-'+arr[2]+' '+arr[3];
		newTweet.created_at_date = pushdate;
	   
		newTweet.save(function (err, newTweet) {
		if (err) 
			throw(err);
		
		});
		
	}
   }
		
});
	
	

	
var server = app.listen(8080, function () {
	console.log('listning at port 8080');
});