var Twit = require('twit');
var config = require('./config');
var http = require('http');
var url = require('url');
var fs = require('fs');
var express = require('express');
var app= express();
var mongoose = require('mongoose');
var Twits = require('./schema/twits');

var filter=require('./filterController');


//Set up default mongoose connection
mongoose.connect('mongodb://localhost:27017/twiterdb');
// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

db.once('open', function() {
  // we're connected!
  
	//set up twitter connection.
	var T = Twit(config);
	app.use('/filter',filter);
	
	
	
});

var server = app.listen(8080, function () {
	console.log('listning at port 8080');
});
